import uvicorn
from fastapi import Depends, FastAPI, HTTPException
from sqlalchemy.orm import Session
from starlette import status

from src import database, models, schemas
from src.crud import create_user, get_current_user
from src.database import engine
from src.gen_token import create_access_token, verify_access_token
from src.utils import hash_password, verify_password

models.Base.metadata.create_all(bind=engine)

app = FastAPI()


@app.post("/token/")
def token(user: schemas.UserLoginSchema, db: Session = Depends(database.get_db)):
    db_user = get_current_user(user, db)
    if not db_user:
        user.password = hash_password(user.password)
        db_user = create_user(user, db)
    else:
        if not verify_password(user.password, db_user.password):
            raise HTTPException(status_code=status.HTTP_401_UNAUTHORIZED,
                                detail="Неверный логин или пароль")

    token = create_access_token(data={"user_id": db_user.id})
    return token


@app.post('/payment/')
def pays(token: str):
    payload = verify_access_token(token)

    return payload


if __name__ == "__main__":
    uvicorn.run(app)
