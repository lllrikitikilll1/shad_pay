from sqlalchemy.orm import Session

from src import models, schemas


def get_current_user(user: schemas.UserLoginSchema, db: Session):
    user = db.query(models.User).filter(models.User.login == user.login).first()
    return user


def create_user(user: schemas.UserLoginSchema, db: Session):
    db_user = models.User(login=user.login, password=user.password)
    db.add(db_user)
    db.commit()
    db.refresh(db_user)
    return db_user
